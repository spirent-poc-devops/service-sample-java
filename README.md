# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> <br/> Sample SpringBoot Microservice

This is a sample "Hello World" microservice in SpringBoot to demonstrate dockerized build and test processes and automated Gitlab CI/CD pipelines

## Develop

For development you shall install the following prerequisites:
* Open JDK 16
* Maven 3
* Visual Studio Code or another IDE of your choice
* Docker

Make sure you have SSH private key in you `~/.ssh` directory.
That key shall be regirested in GitHub with *SSO enabled*.

Compile the microservice:
```bash
mvn clean package
```

Before running tests launch infrastructure services and required microservices:
```bash
docker-compose -f ./docker-compose.dev.yml
```

Run automated tests:
```bash
mvn test
```

Before committing changes run dockerized build and test as:
```bash
./build.ps1
./test.ps1
./package.ps1
./run.ps1
./clean.ps1
```

## Contacts

This microservice was created and currently maintained by *Sergey Seroukhov*.
