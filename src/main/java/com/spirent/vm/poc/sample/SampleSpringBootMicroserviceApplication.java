package com.spirent.vm.poc.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleSpringBootMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleSpringBootMicroserviceApplication.class, args);
	}

}
