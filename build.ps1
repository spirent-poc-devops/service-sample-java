#!/usr/bin/env pwsh

# Set-StrictMode -Version latest
# $ErrorActionPreference = "Stop"

# Get component data and set necessary variables
$component = Get-Content -Path "component.json" | ConvertFrom-Json

# Get buildnumber from github actions
if ($env:GITLAB_RUN_NUMBER -ne $null) {
    $component.build = $env:GITLAB_RUN_NUMBER
    Set-Content -Path "component.json" -Value $($component | ConvertTo-Json)
}

$buildImage="$($component.registry)/$($component.name):$($component.version)-$($component.build)-build"
$container=$component.name

# Remove build files
if (Test-Path "dist") {
    Remove-Item -Recurse -Force -Path "dist"
}

# Copy private keys to access git repo
if (-not (Test-Path -Path "docker/id_rsa")) {
    if ($env:GIT_PRIVATE_KEY -ne $null) {
        Set-Content -Path "docker/id_rsa" -Value $env:GIT_PRIVATE_KEY
    } else {
        Copy-Item -Path "~/.ssh/id_rsa" -Destination "docker"
    }
}

# Build docker image
docker build -f docker/Dockerfile.build -t $buildImage .

# Create and copy compiled files, then destroy
docker create --name $container $buildImage

# Recreate target folder and copy it from the build image
Get-ChildItem -Path "." -Include "target" -Recurse | foreach($_) { Remove-Item -Force -Recurse $_.FullName }
docker cp "$($container):/app/target" .

# Copy build artifacts over to dist folder
New-Item -ItemType directory -Path "./dist"
Copy-Item -Path "./target/*.jar" -Destination "./dist"
Copy-Item -Path "./target/*.jar" -Destination "./dist/main.jar"

# Remove the build container
docker rm $container

if (!(Test-Path ./dist/main.jar)) {
    Write-Host "dist folder doesn't exist in root dir. Build failed. Watch logs above."
    exit 1
}
